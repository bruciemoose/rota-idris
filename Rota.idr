import Data.Vect
import Data.SortedSet

%default total

MinShiftDuration : Nat
MinShiftDuration = 1

MaxShiftDuration : Nat
MaxShiftDuration = 8

StartOfDay : Nat
StartOfDay = 7

EndOfDay : Nat
EndOfDay = 27

MaxHoursPerWeek : Nat
MaxHoursPerWeek = 40

data Day
  = Monday
  | Tuesday
  | Wednesday
  | Thursday
  | Friday
  | Saturday
  | Sunday

Eq Day where
  (==) Monday Monday = True
  (==) Tuesday Tuesday = True
  (==) Wednesday Wednesday = True
  (==) Thursday Thursday = True
  (==) Friday Friday = True
  (==) Saturday Saturday = True
  (==) Sunday Sunday = True
  (==) _ _ = False

dayFromString : String -> Either String Day
dayFromString "mo"        = Right Monday
dayFromString "monday"    = Right Monday
dayFromString "tu"        = Right Tuesday
dayFromString "tuesday"   = Right Tuesday
dayFromString "we"        = Right Wednesday
dayFromString "wednesday" = Right Wednesday
dayFromString "th"        = Right Thursday
dayFromString "thursday"  = Right Thursday
dayFromString "fr"        = Right Friday
dayFromString "friday"    = Right Friday
dayFromString "sa"        = Right Saturday
dayFromString "saturday"  = Right Saturday
dayFromString "su"        = Right Sunday
dayFromString "sunday"    = Right Sunday
dayFromString _           = Left "Specify day as e.g. monday or mo"

record Shift where
  constructor MkShift
  name : String
  day : Day
  start : Nat
  duration : Nat

end : Shift -> Nat
end s = start s + duration s

overlapsWith : Shift -> Shift -> Bool
overlapsWith x y
  = day x == day y && (start x < end y && end x > start y)

StartsLateEnough : Shift -> Type
StartsLateEnough shift = StartOfDay `LTE` start shift

startsLateEnough : (shift : Shift) -> Dec (StartsLateEnough shift)
startsLateEnough shift = StartOfDay `isLTE` start shift

DurationShortEnough : Shift -> Type
DurationShortEnough shift = duration shift `LTE` MaxShiftDuration

durationShortEnough : (shift : Shift) -> Dec (DurationShortEnough shift)
durationShortEnough shift = duration shift `isLTE` MaxShiftDuration

DurationLongEnough : Shift -> Type
DurationLongEnough shift = MinShiftDuration `LTE` duration shift

durationLongEnough : (shift : Shift) -> Dec (DurationLongEnough shift)
durationLongEnough shift = MinShiftDuration `isLTE` duration shift

DoesNotOverrun : Shift -> Type
DoesNotOverrun shift = start shift `LTE` (EndOfDay `minus` duration shift)

doesNotOverrun : (shift : Shift) -> Dec (DoesNotOverrun shift)
doesNotOverrun shift = start shift `isLTE` (EndOfDay `minus` duration shift)

mutual
  data WeekRota : Type where
       Nil : WeekRota
       (::) : (shift : Shift)
           -> (existingRota : WeekRota)
           -> {auto prfLateEnough : StartsLateEnough shift}
           -> {auto prfShortEnough : DurationShortEnough shift}
           -> {auto prfLongEnough : DurationLongEnough shift}
           -> {auto prfDoesNotOverrun : DoesNotOverrun shift}
           -> {auto prfShiftFits : ShiftFits shift existingRota}
           -> {auto prfNoOverlap : NoOverlap shift existingRota}
           -> {auto prfNoExistingShift : NoShiftForPersonThatDay shift existingRota}
           -> WeekRota

  rotaHoursForName : (workerName : String) -> WeekRota -> Nat
  rotaHoursForName workerName [] = 0
  rotaHoursForName workerName (shift :: rota)
    = rotaHoursForName workerName rota +
      if name shift == workerName then duration shift else 0

  ShiftFits : Shift -> WeekRota -> Type
  ShiftFits shift rota = rotaHoursForName (name shift) rota + duration shift
                         `LTE` MaxHoursPerWeek

  shiftFits : (shift : Shift) -> (rota : WeekRota) -> Dec (ShiftFits shift rota)
  shiftFits shift rota = rotaHoursForName (name shift) rota + duration shift
                         `isLTE` MaxHoursPerWeek

  hasOverlappingShift : Shift -> WeekRota -> Bool
  hasOverlappingShift shift [] = False
  hasOverlappingShift shift (s :: rota)
    = overlapsWith shift s || hasOverlappingShift shift rota

  NoOverlap : Shift -> WeekRota -> Type
  NoOverlap shift existingRota = hasOverlappingShift shift existingRota = False

  noOverlap : (shift : Shift) -> (rota : WeekRota) -> Dec (NoOverlap shift rota)
  noOverlap shift rota = hasOverlappingShift shift rota `decEq` False

  existingDayShift : Shift -> WeekRota -> Bool
  existingDayShift proposedShift [] = False
  existingDayShift proposedShift (s :: rota)
    = (day proposedShift == day s && name proposedShift == name s)
      || existingDayShift proposedShift rota

  NoShiftForPersonThatDay : Shift -> WeekRota -> Type
  NoShiftForPersonThatDay proposedShift rota
    = existingDayShift proposedShift rota = False

  noShiftForPersonThatDay : (proposedShift : Shift)
                         -> (rota : WeekRota)
                         -> Dec (NoShiftForPersonThatDay proposedShift rota)
  noShiftForPersonThatDay proposedShift rota
    = existingDayShift proposedShift rota `decEq` False


namesInRota : WeekRota -> SortedSet String
namesInRota [] = empty
namesInRota (shift :: rota) = insert (name shift) (namesInRota rota)

Show WeekRota where
  show rota
    = showR "" (SortedSet.toList $ namesInRota rota) rota
      where
        formatLine : (name : String) -> (hours : Nat) -> String
        formatLine name hours
          = name ++ ": " ++ show hours ++ " hours\n"

        showR : (acc : String) -> (names : List String) -> WeekRota -> String
        showR acc [] r = acc
        showR acc (name :: names) r
          = showR (acc ++ formatLine name (rotaHoursForName name r))
                  names
                  r

addShift : Shift -> WeekRota -> (String, WeekRota)
addShift shift rota
  = either rollBack (commitWith "Shift added") $ do
           "Shift starts too early"
             `unless` startsLateEnough shift

           "Shift too long"
             `unless` durationShortEnough shift

           "Shift not long enough"
             `unless` durationLongEnough shift

           "Shift overruns"
             `unless` doesNotOverrun shift

           "Too many hours worked in week"
             `unless` shiftFits shift rota

           "Only one member of staff on shift at a time"
             `unless` noOverlap shift rota

           "Staff can only work one shift per day"
             `unless` noShiftForPersonThatDay shift rota

           pure $ shift :: rota
    where
      rollBack = flip MkPair rota
      commitWith = MkPair

      unless : String -> Dec p -> Either String p
      unless _ (Yes prf) = Right prf
      unless msg (No _) = Left msg

showRota : WeekRota -> (String, WeekRota)
showRota rota = (show rota, rota)

data Command : Type where
  AddShift : Shift -> Command
  ShowRota : Command

parse : List String -> Either String Command
parse ("add" :: name :: day :: start :: duration :: _)
  = pure $ AddShift !(parsedShift name day start duration)
    where
      natify : String -> Either String Nat
      natify s = if all isDigit (unpack s)
                 then Right $ cast s
                 else Left "Non-numeric value provided"

      parsedShift : (strName     : String)
                 -> (strDay      : String)
                 -> (strStart    : String)
                 -> (strDuration : String)
                 -> Either String Shift
      parsedShift strName strDay strStart strDuration
        = pure $ MkShift strName
                         !(dayFromString strDay)
                         !(natify strStart)
                         !(natify strDuration)

parse ("show" :: _)
  = pure ShowRota

parse _
  = Left "Unknown command"

processInput : WeekRota -> String -> Maybe (String, WeekRota)
processInput rota input
  = case parse (split (== ' ') input) of
      Left msg =>
        Just (msg, rota)
      Right (AddShift shift) =>
        Just $ addShift shift rota
      Right ShowRota =>
        Just $ showRota rota

partial
main : IO ()
main = replWith [] "\n> " processInput

-- Local Variables:
-- idris-load-packages: ("contrib")
-- End:
