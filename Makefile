.POSIX:
.SUFFIXES:

rota: Rota.idr
	idris \
	--package contrib \
	--output $@ \
	$^
